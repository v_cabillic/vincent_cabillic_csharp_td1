﻿using System;

namespace AnalyseDeCode_1
{

    /// <summary>
    /// Le but de l’exercice consiste à faire une analyse du code présenté.
    /// Ceci consiste à décrire le mécanisme.
    /// 1-	Ajouter les commenter directement dans le code.
    /// •	Commentaire des méthodes et classes /// 
    /// •	Commentaire des mécanismes //
    /// 2-	Un delegate est déclaré de manière explicite.L’idée est de le rendre implicite en utilisant les mécanique vu en session.
    /// </summary>
    /// 

    /// Classe principal 
    class Program
    {
        static void Main(string[] args)
        {
            // Déclaration, instanciation et affectation d'un objet "myNumber" héritant(???) de la classe Number.
            Number myNumber = new Number(100000);
            // Lancement de la method PrintMoney.
            myNumber.PrintMoney();
            // Lancement de la method PrintNumber.
            myNumber.PrintNumber();
        }
    }

    /// <summary>
    /// Classe Number
    /// </summary>
    class Number
    {
        /// <summary>
        /// Déclaration d'un objet PrintHelper.
        /// </summary>        
        private PrintHelper _printHelper;

        /// <summary>
        /// Constructeur de la classe Number.
        /// </summary>
        /// <param name="val">Un nombre entier</param>
        public Number(int val)
        {
            // Affectation de la propriété value avec le paramètre du constructeur.
            _value = val;
            // Instanciation de l'objet _printHelper.
            _printHelper = new PrintHelper();
            // Abonnement de la fonction "printHelper_beforePrintEvent" à l'évènement "beforePrintEvent" de l'objet "_printHelper".
            // Un pointeur sur la fonction "printHelper_beforePrintEvent" est ajouté à la liste de l'event "beforePrintEvent".
            _printHelper.beforePrintEvent += printHelper_beforePrintEvent;
        }

        /// <summary>
        /// Methode affichant un message.
        /// </summary>
        void printHelper_beforePrintEvent()
        {
            Console.WriteLine("BeforPrintEventHandler: PrintHelper is going to print a value");
        }

        // Décalration de variable _value de type entier.
        private int _value;

        /// <summary>
        /// Paramétrage de la properties value.
        /// </summary>
        public int Value
        {
            get { return _value; }
            set { _value = value; }
        }

        /// <summary>
        /// Déclaration d'une method permettant d'afficher une valeur au format monétaire.
        /// </summary>
        public void PrintMoney()
        {
            _printHelper.PrintMoney(_value);
        }

        /// <summary>
        /// Déclaration d'une method permettant d'afficher une valeur au format nombre.
        /// </summary>
        public void PrintNumber()
        {
            _printHelper.PrintNumber(_value);
        }
    }

    /// <summary>
    /// Classe PrintHelper. Met a disposition des methods pour l'affichage de valeur numérique en différent format.
    /// </summary>
    public class PrintHelper
    {
        // Déclaration d'un delegate beforePrint.
        public delegate void BeforePrint();
        // Déclaration d'un évènement beforePrint.
        public event BeforePrint beforePrintEvent;

        /// <summary>
        ///  Constructeur de la classe PrintHelper.
        /// </summary>
        public PrintHelper() { }

        /// <summary>
        /// Affiche un nombre.
        /// </summary>
        /// <param name="num">Un nombre</param>
        public void PrintNumber(int num)
        {
            // Si au moins un event est abonné, exécute les delegate affectés
            if (beforePrintEvent != null)
                beforePrintEvent();

            // Affiche le valeur spécifié en premier paramètre.
            // La valeur est affiché avec une longueur maximal de 12 caractères, aligné à gauche et afficché au format Number (N).
            Console.WriteLine("Number: {0,-12:N0}", num); 
        }

        /// <summary>
        /// Affiche un nombre au format général décimal.
        /// </summary>
        /// <param name="dec">Un nombre entier</param>
        public void PrintDecimal(int dec)
        {
            if (beforePrintEvent != null)
                beforePrintEvent();

            // Affiche la valeur spécifié en premier paramètre.
            // La valeur est au format General (G).
            Console.WriteLine("Decimal: {0:G}", dec);
        }

        /// <summary>
        /// Affiche une valeur monétaire.
        /// </summary>
        /// <param name="money">Une valeur monétaire</param>
        public void PrintMoney(int money)
        {
            if (beforePrintEvent != null)
                beforePrintEvent();
            
            // Affiche la valeur spécifié en premier paramètre.
            // La valeur est au format Devise (C).
            Console.WriteLine("Money: {0:C}", money);
        }

        /// <summary>
        /// Affiche une température.
        /// </summary>
        /// <param name="num">Une température</param>
        public void PrintTemperature(int num)
        {
            if (beforePrintEvent != null)
                beforePrintEvent();

            // Affiche la valeur spécifié en premier paramètre.
            // La valeur est affiché avec une longueur maximal de 4 caractères, aligné à droite et au format Numérique (N1).
            Console.WriteLine("Temperature: {0,4:N1} F", num);
        }

        /// <summary>
        /// Affiche un nombre hexadécimal.
        /// </summary>
        /// <param name="dec">Un nombre entier</param>
        public void PrintHexadecimal(int dec)
        {
            if (beforePrintEvent != null)
                beforePrintEvent();

            // Affiche la valeur spécifié en premier paramètres.
            // La valeur est affiché au format hexadécimal.
            Console.WriteLine("Hexadecimal: {0:X}", dec);
        }
    }
}
