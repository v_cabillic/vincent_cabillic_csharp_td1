﻿using System;

/// <summary>
/// Author : Vincent Cabillic
/// Created : 01/04/2020
/// contact : vincent.cabillic@viacesi.fr
/// 
/// Affichage de la plus grande valeur parmis 3 valeurs saisies.
/// </summary>
namespace Exercice_5_DivisionNombre
{
    class Program
    {
        /// <summary>
        /// Ensemble de valeurs entières.
        /// </summary>
        static int[] valeurs = new int[] { 48, 5012 };

        static void Main(string[] args)
        {
            // Boucle à traver le tableau d'entier.
            for (int i = 0; i < valeurs.Length; i++)
            {
                Console.WriteLine("Le {0}e nombre est : {1}", i, valeurs[i]);
            }

            // Calcul la division des valeurs du tableau.
            double division = (double) valeurs[0] / (double) valeurs[1];

            // Affiche la division des deux nombres du tableau.
            Console.WriteLine("La divsion de ces nombres est {0}", division);
        }
    }
}
