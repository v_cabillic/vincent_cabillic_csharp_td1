﻿using System;

/// <summary>
/// Author : Vincent Cabillic
/// Created : 01/04/2020
/// contact : vincent.cabillic@viacesi.fr
/// 
/// Calcul de la surface et du volume d'une sphère à partir de son rayon.
/// </summary>
namespace Exercice_1_VolumeEtSurfaceSphere
{
    class Program
    {
        static void Main(string[] args)
        {
            // Rayon de la sphère
            double rayon = 7.5;

            // Déclaration des éléments à calculer
            double surface, volume;

            // Calcul de la surface de la sphère
            surface = 4d * Math.PI * Math.Pow(rayon, 2);

            // Calcul du volume de la sphère
            volume = (4d * Math.PI * Math.Pow(rayon, 3)) / 3d;

            // Affichage des résultats
            Console.WriteLine("Une sphère de rayon {0} à :\n\t - une surface de : {1}\n\t - un volume de   : {2}", rayon, surface, volume);
        }
    }
}
