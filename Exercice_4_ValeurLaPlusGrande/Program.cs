﻿using System;
using System.Collections.Generic;

/// <summary>
/// Author : Vincent Cabillic
/// Created : 01/04/2020
/// contact : vincent.cabillic@viacesi.fr
/// 
/// Affichage de la plus grande valeur parmis 3 valeurs saisies.
/// </summary>
namespace Exercice_4_ValeurLaPlusGrande
{
    class Program
    {
        /// <summary>
        /// Tableau de 3 valeurs.
        /// </summary>
        static int[] valeurs = new int[] { 12, - 150, 8 };

        static void Main(string[] args)
        {
            int entierLePlusGrand = valeurs[0];

            // Boucle à traver le tableau d'entier.
            for(int i = 0; i < valeurs.Length; i++)
            {
                Console.WriteLine("Le {0}e nombre est : {1}", i, valeurs[i]);
                // Si le nombre en cours est plus grand que le précédent l'affecte à la variable.
                if (valeurs[i] > entierLePlusGrand)
                {
                    entierLePlusGrand = valeurs[i];
                }
            }

            // Affiche l'entier le plus grand.
            Console.WriteLine("Le nombre le plus grand est : {0}", entierLePlusGrand);
        }
    }
}
