﻿using System;

/// <summary>
/// Author : Vincent Cabillic
/// Created : 01/04/2020
/// contact : vincent.cabillic@viacesi.fr
/// 
/// Calcul mathématiques simples.
/// </summary>
namespace Exercice_2_CalculsMathematiques
{
    class Program
    {
        // Déclaration des variables de calculs
        const int x = 10;
        const int y = 5;

        static void Main(string[] args)
        {
            // Calcul de somme
            int somme = y + 3;
            Console.WriteLine("La somme de {0} et 5 est {1}", y, somme);

            // Calcul de soustraction
            int soustraction = y - 3;
            Console.WriteLine("La soustraction de {0} et 3 est {1}");

            // Calcul de multiplication
            int multiplication = y * 5;
            Console.WriteLine("La multiplication de {0} et 5 est {1}");

            // Calcul de division
            int division = x / y;
            Console.WriteLine("La division euclidienne de {0} et {1} est {2}");

            // Calcul de modulo
            int modulo = x % y;
            Console.WriteLine("Le reste de la division euclidienne de {0} par {1} est {2}");
        }
    }
}
