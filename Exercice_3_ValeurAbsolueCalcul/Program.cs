﻿using System;

/// <summary>
/// Author : Vincent Cabillic
/// Created : 01/04/2020
/// contact : vincent.cabillic@viacesi.fr
/// 
/// Calcul valeur absolue.
/// </summary>
namespace Exercice_3_ValeurAbsolueCalcul
{
    class Program
    {
        static void Main(string[] args)
        {
            // Déclaration du nombre à calculer
            double nombre;

            // Demande d'entrée, pour calculer la valeur absolue d'un nombre
            Console.WriteLine("Entrez un nombre réel : ");
            nombre = Convert.ToDouble(Console.ReadLine());

            // Calcul et affichage de la valeur absolue du nombre
            double valeurAbsolue = calculAbsolue(nombre);
            Console.WriteLine("La valeur absolue de {0} est {1}.", nombre, valeurAbsolue);
        }

        /// <summary>
        /// Calcul la valeur absolue d'un nombre.
        /// </summary>
        /// <param name="nombre">Un nombre réel</param>
        /// <returns>La valeur absolue du nombre</returns>
        private static double calculAbsolue(double nombre)
        {
            if (nombre < 0)
            {
                return nombre * -1d;
            }
            else 
            {
                return nombre;
            }
        }
    }
}
